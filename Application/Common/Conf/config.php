<?php
 return array(
 	'wsIp' => "",//聊天服务器
	'registerAddress' => "",//聊天服务器
	'SHOW_PAGE_TRACE'=>false,
	'app_debug'=>false,
	'app_trace'=>false,
	'VAR_PAGE'=>'p',
	'PAGE_SIZE'=>15,
	'DB_TYPE'=>'mysql',
	'DB_HOST'=>'localhost',
	'DB_NAME'=>'tourismbase',
	'DB_USER'=>'root',
	'DB_PWD'=>'root',
	'DB_PREFIX'=>'oto_',
	'DEFAULT_CITY' => '440100',
 	'URL_MODEL' => 2,
	'DATA_CACHE_SUBDIR'=>true,
        'DATA_PATH_LEVEL'=>2,
	'SESSION_PREFIX' => 'oto_mall',
        'COOKIE_PREFIX'  => 'oto_mall',
	'LOAD_EXT_CONFIG' => 'ext_config',
 	'MODULE_ALLOW_LIST' => array('Home','Admin','Wx','Api','Go','Dealer','Reseller','Operator'),    // 允许访问的模块列表
 	//图片上传相关信息
	'VIEW_ROOT_PATH'        =>   '/Upload/',
	);
?>
