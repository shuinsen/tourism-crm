<?php
namespace Operator\Model;

use Think\Model;

class StaffModel extends Model
{
    //自定义条件
    public function getSomeStaff($condition, $pid = 0)
    {
        $where = [
            'operator_id'      => session('operator_user.operator_id'),
            'reseller_flag'    => 1,
            'is_review'        => 1,
            'reseller_status'  => 1,
        ];

        $where = array_merge($where, $condition);
        $staff = M('operator_line_reseller')
            ->field('reseller_id, reseller_name')
            ->where($where)
            ->select();
        return $staff;
    }
    //获取分销商
    public function getReseller()
    {
        // $operator_id = session('operator_user.operator_id');
        return M('operator_line_reseller')
            ->field('reseller_id, reseller_name')
            ->where([
                'operator_id'      => session('operator_user.operator_id'),
                'pid'              => 0,
                'reseller_flag'    => 1,
                'is_review'        => 1,
                'reseller_status'  => 1,
                'shop_or_reseller' => 2,
            ])
            ->select();
    }

    //获取直营门店
    public function getShop()
    {
        // $operator_id = session('operator_user.operator_id');
        return M('operator_line_reseller')
            ->field('reseller_id, reseller_name, pid')
            ->where([
                'operator_id'      => session('operator_user.operator_id'),
                'pid'              => 0,
                'reseller_flag'    => 1,
                'is_review'        => 1,
                'reseller_status'  => 1,
                'shop_or_reseller' => 1,
            ])
            ->select();
    }

    //获取所有店铺
    public function getAllShop()
    {
        // $operator_id = session('operator_user.operator_id');
        return M('operator_line_reseller')
            ->field('reseller_id, reseller_name')
            ->where([
                'operator_id'      => session('operator_user.operator_id'),
                'pid'              => 0,
                'reseller_flag'    => 1,
                'is_review'        => 1,
                'reseller_status'  => 1,
            ])
            ->select();
    }

}
