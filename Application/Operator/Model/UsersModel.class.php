<?php
namespace Operator\Model;
/**
 * ModelName
 */
class UsersModel extends BaseModel{

    protected $tableName = 'operator';

    // 自动验证
    protected $_validate=array(
        array('operator_name','require','用户名必须',0,'',3), // 验证字段必填
        array('operator_phone','checkPhone','手机号格式不正确!',0,'callback',3), // 验证字段必填
        array('operator_phone','checkPhoneExists','手机号格式不正确!',0,'callback',3), // 验证字段必填
        array('operator_account','require','登录帐号必须填写',1,'',3), // 验证字段必填
        array('operator_account','checkAccount','帐号已经存在!',1,'callback',3), // 验证字段必填
    );

    /**
     * 判断手机号是否正确
     */
    public  function  checkPhone(){
        $operator_phone=I('operator_phone');
        if(preg_match("/^1[34578]{1}\d{9}$/",$operator_phone)){
           return true;
        }else{
            return false;
        }
    }

    /**
     * 判断手机号是否重复
     */
    public  function checkPhoneExists(){
        $id=I('id');
        $map['operator_phone']=I('operator_phone');
        $map['operator_flag']=1;
        $info=$this->where($map)->find();
        if($info&&$info['operator_id']!=$id){
            return false;
        }
        return true;
    }


    //判断用户帐号是否重复
    public function checkAccount(){
        $id=I('id');
        $map['operator_account']=I('operator_account');
        $map['operator_flag']=1;
        $info=$this->where($map)->find();
        if($info&&$info['operator_id']!=$id){

            return false;
        }
        return true;
    }

    // 自动完成
    protected $_auto=array(
        //array('login_pwd','md5',1,'function') , // 对password字段在新增的时候使md5函数处理
        array('create_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
        array('pid','setPid',1,'callback'), //设置父ID
        array('operator_type','1'), // 默认是国内
    );


    public  function  setPid(){
        if(session('IS_ADMIN')){
           return  session('operator_user.operator_id');
        }
        return session('operator_user.pid');
    }


    /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证

        if(!$data['is_store']){
            unset($data['store_name']);
        }

        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            if($data['login_pwd']){
                $data['login_secret']=mt_rand(1000,9999);
                $data['login_pwd']=md5( $data['login_pwd'].$data['login_secret']);
            }
            unset($data['id']);
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            if($data['login_pwd']){
                $data['login_secret']=mt_rand(1000,9999);
                $data['login_pwd']=md5( $data['login_pwd'].$data['login_secret']);
            }

            if(session('IS_ADMIN')){
                if($map['id']==session('operator_user.operator_id')){
                    unset($data['pid']);
                }
            }

            unset($data['id']);
            $result=$this
                ->where(array($map))
                ->save($data);
            return $result;
        }
    }

    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        die('禁止删除用户');
    }

        /**
     * 更改用户状态
     */
     public  function changeUserStatus($data){
         $uid=intval($data['uid']);
         if(!$uid){
             return ['status'=>-1,'msg'=>'操作失败!'];
         }
         $res=M('operator')->where(['operator_id'=>$uid])->save(['operator_status'=>$data['status']]);
         if($res){
             return ['status'=>1,'msg'=>'操作成功!'];
         }
         return ['status'=>0,'msg'=>'操作失败!'];
     }



}
