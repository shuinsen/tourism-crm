<?php
namespace Reseller\Model;

use Think\Model;

class StaffModel extends Model
{
    //自定义条件
    public function getSomeStaff($condition, $pid = 0)
    {
        $where = [
            'operator_id'      => session('reseller_user.operator_id'),
            'reseller_flag'    => 1,
            'is_review'        => 1,
            'reseller_status'  => 1,
            'shop_or_reseller' => session('reseller_user.shop_or_reseller'),
        ];

        $where = array_merge($where, $condition);
        $staff = M('operator_line_reseller')
            ->field('reseller_id, reseller_name')
            ->where($where)
            ->select();
        if (!$pid) {
            $staff[] = [
                'reseller_id'   => session('reseller_user.reseller_id'),
                'reseller_name' => session('reseller_user.reseller_name'),
            ];
        }
        return $staff;
    }

    /**
     * 返回该分销商店员编号与姓名
     */
    public function getStaff()
    {
        // $key = md5('staff');
        // if(S($key)) return S(md5('staff'));
        //有pid表示店员登录
        if (session('reseller_user.pid')) {
            $pid   = session('reseller_user.pid');
            $staff = M('operator_line_reseller force index(pid)')
                ->field('reseller_id, reseller_name ')
                ->where(" (reseller_id = '{$pid}' or pid = '{$pid}') and is_review = 1 and reseller_flag = 1 ")
                ->select();

            //店长登录
        } else {
            $pid   = session('reseller_user.reseller_id');
            $staff = M('operator_line_reseller force index(pid)')
                ->field('reseller_id, reseller_name ')
                ->where("  pid = '{$pid}' and is_review = 1 and reseller_flag = 1 ")
                ->select();
            $staff[] = [
                'reseller_id'   => session('reseller_user.reseller_id'),
                'reseller_name' => session('reseller_user.reseller_name'),
            ];
        }
        // $staff = array_combine(array_map('array_shift', $staff), $staff);
        // S($key, $staff, 43200);
        return $staff;
    }
}
