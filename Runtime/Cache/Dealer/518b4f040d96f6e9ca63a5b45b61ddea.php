<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->

    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        .required{color:#FF9966}
        .thumbImg{height:200px;}
    </style>
    
    <link rel="stylesheet" href="/Public/statics/layui/css/layui.css"  media="all">
    <style>
      th{text-align:center;}
        h3{font-size:18px; padding:10px;}
    </style>

</head>
<body>

    <div class="page-header"><h1><i class="fa fa-home"></i> 数据统计</h1></div>
     <div class="container-fluid">
         <!--线形图及饼状图-->
         <div class="row" style="width:98%;overflow: hidden;margin:0 auto;">
             <div class=" col-md-6 col-xs-6">
                 <form class="form-horizontal" method="post" action="<?php echo U('welcome');?>">
                     <div class="form-group">
                         <label for="toDay" class="col-sm-2 control-label">日期选择:</label>
                         <div class="col-sm-8">
                             <input type="text" name="toDay" value="<?php echo ($toDay); ?>" class="form-control-" id="toDay" placeholder="请选择日期">
                         </div>
                         <div class="col-sm-2">
                             <input type="submit" class="btn btn-primary" value="查询">
                         </div>
                     </div>
                 </form>
                <div id="pie"></div>
             </div>
             <div class=" col-md-6 col-xs-6">
                 <form class="form-horizontal" method="post" action="<?php echo U('welcome');?>">
                     <div class="form-group">
                         <label for="inputEmail3" class="col-sm-2 control-label">按年/月筛选:</label>
                         <div class="col-sm-3">
                             <select name="type" id="type">
                                 <option <?php if($type == 1): ?>selected<?php endif; ?> value="1">==按年查看==</option>
                                 <option <?php if($type == 2): ?>selected<?php endif; ?>  value="2">==按月查看==</option>
                             </select>

                         </div>
                         <div class="col-sm-3">
                             <select name="year" id="year" style="display:<?php if($type == 1): ?>block;<?php else: ?>none;<?php endif; ?>" >
                                 <?php if(is_array($yearList)): $i = 0; $__LIST__ = $yearList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($year == $vo['y']): ?>selected<?php endif; ?> value="<?php echo ($vo["y"]); ?>"><?php echo ($vo["y"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                             </select>
                             <select name="month" id="month" style="display:<?php if($type == 2): ?>block;<?php else: ?>none;<?php endif; ?>"  >
                                 <option value="1" <?php if($month == 1): ?>selected<?php endif; ?> >1月</option>
                                 <option value="2" <?php if($month == 2): ?>selected<?php endif; ?>>2月</option>
                                 <option value="3" <?php if($month == 3): ?>selected<?php endif; ?>>3月</option>
                                 <option value="4" <?php if($month == 4): ?>selected<?php endif; ?>>4月</option>
                                 <option value="5" <?php if($month == 5): ?>selected<?php endif; ?>>5月</option>
                                 <option value="6" <?php if($month == 6): ?>selected<?php endif; ?>>6月</option>
                                 <option value="7" <?php if($month == 7): ?>selected<?php endif; ?>>7月</option>
                                 <option value="8" <?php if($month == 8): ?>selected<?php endif; ?>>8月</option>
                                 <option value="9" <?php if($month == 9): ?>selected<?php endif; ?>>9月</option>
                                 <option value="10" <?php if($month == 10): ?>selected<?php endif; ?>>10月</option>
                                 <option value="11" <?php if($month == 11): ?>selected<?php endif; ?>>11月</option>
                                 <option value="12" <?php if($month == 12): ?>selected<?php endif; ?>>12月</option>
                             </select>
                         </div>
                         <div class="col-sm-2">
                             <input type="submit" class="btn btn-primary" value="查询">
                         </div>
                     </div>
                 </form>
                <div id="strip">
                </div>
             </div>
         </div>
         <!--线形图及饼状图结束-->

         <!--最新消息-->
         <h3>最新消息</h3>
         <div class="newMess">
             <table class="table table-striped table-bordered table-hover table-condensed text-center">
                 <tbody>
                 <tr>
                     <th>序号</th>
                     <th>线路名称</th>
                     <th>线路编号</th>
                     <th>给运营商结算单价</th>
                     <th>发布人</th>
                     <th>运营商名称</th>
                     <th>合作状态</th>
                     <th>建立合作时间</th>
                 </tr>
                <?php if(is_array($messList)): $i = 0; $__LIST__ = $messList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                     <td><?php echo ($i); ?></td>
                     <td><?php echo ($vo["line_name"]); ?></td>
                     <td><?php echo ($vo["line_sn"]); ?></td>
                     <td>成人<?php echo ($vo["adult_money"]); ?>;儿童<?php echo ($vo["child_money"]); ?>;</td>
                     <td><?php echo ($vo["supplier_name"]); ?></td>
                     <td><?php echo ($vo["operator_name"]); ?></td>
                     <td>
                        <?php switch($vo["partner_status"]): case "0": ?><label class="label label-primary">申请合作中 </label><?php break;?>
                            <?php case "1": ?><label class="label label-success">合作中</label><?php break;?>
                            <?php case "-1": ?><label class="label label-danger">运营商拒绝合作</label><?php break;?>
                            <?php case "-2": ?><label class="label label-danger">供应商中止合作</label><?php break; endswitch;?>
                     </td>
                     <td><?php echo (date("Y-m-d",$vo["partner_time"])); ?></td>
                 </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                 </tbody>
             </table>
         </div>
         <!--最新消息结束-->
         <!--最新订单-->
         <h3>最新订单</h3>
         <div class="newMess">
             <table class="table table-striped table-bordered table-hover table-condensed text-center">
                 <tbody>
                 <tr >
                     <th>序号</th>
                     <th>线路名称</th>
                     <th>线路编号</th>
                     <th>团号</th>
                     <!--<th>结算总额</th>-->
                     <th>下单时间</th>
                     <th>出团时间</th>
                    <!-- <th>余位</th>-->
                     <th>预计收客</th>
                     <th>状态</th>
                     <th>操作</th>
                 </tr>
                 <?php if(is_array($orderList)): $i = 0; $__LIST__ = $orderList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                         <td><?php echo ($i); ?></td>
                         <td><?php echo ($vo["line_name"]); ?></td>
                         <td><?php echo ($vo["line_sn"]); ?></td>
                         <td><?php echo ($vo["group_num"]); ?></td>
                         <!--<td><?php echo ($vo["operator_cost"]); ?></td>-->
                         <td><?php echo (date("Y-m-d H:i:s",$vo["create_time"])); ?></td>
                         <td><?php echo (date("Y-m-d",$vo["out_time"])); ?></td>
                         <!--<td>4</td>-->
                         <td><?php echo ($vo["expect_num"]); ?></td>
                         <td>
                             <?php switch($vo["group_status"]): case "0": ?>待处理<?php break;?>
                                <?php case "2": ?>已出团<?php break;?>
                                <?php case "1": ?>已成团<?php break;?>
                                <?php case "-1": ?>不成团<?php break;?>
                                <?php case "3": ?>已回团<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php if(checkAuth('Dealer/Product/groupManage')): ?><a href="<?php echo U('Product/groupManage');?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                             <?php if(checkAuth('Dealer/Finance/financeList')): ?><a href="<?php echo U('Finance/financeList');?>" class="btn btn-xs btn-primary">结算</a><?php endif; ?>
                         </td>
                     </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                 </tbody>
             </table>
         </div>
         <!--最新订单结束-->

         <!--产品审核-->
         <h3>产品审核</h3>
         <div class="newMess">
             <table class="table table-striped table-bordered table-hover table-condensed text-center">
                 <tbody>
                 <tr >
                     <th>供应商名称</th>
                     <th>产品名称</th>
                     <th>供应商编号</th>
                     <th>销售单价</th>
                     <th>合作时间</th>
                     <th>操作人</th>
                     <th>审核状态</th>
                     <th>是否上架</th>
                     <th>操作</th>
                 </tr>
                 <!--酒店-->
                <?php if(is_array($auditList['hotelList'])): $i = 0; $__LIST__ = $auditList['hotelList'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                         <td><?php echo ($vo["supplier_name"]); ?></td>
                         <td>酒店</td>
                         <td><?php echo ($vo["supplier_sn"]); ?></td>
                         <td><?php echo ($vo["room_name"]); ?>: 儿童价：<?php echo ($vo["child_actual_cost"]); ?>元/人<br>成人价：<?php echo ($vo["adult_actual_cost"]); ?>元/人 </td>
                         <td><?php echo (date("Y-m-d",$vo["start_time"])); ?>至<?php echo (date("Y-m-d",$vo["end_time"])); ?></td>
                         <td><?php echo ($vo["audit_name"]); ?></td>
                         <td>
                             <?php switch($vo["is_review"]): case "1": ?>已审核<?php break;?>
                                 <?php case "-1": ?>未审核<?php break;?>
                                 <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php switch($vo["is_shelves"]): case "1": ?>是<?php break;?>
                                 <?php case "-1": ?>否<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php if(checkAuth('Dealer/Shops/hotelProductById')): ?><a href="<?php echo U('Shops/hotelProductById',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                         </td>
                     </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                 <!--景区-->

                 <?php if(is_array($auditList['scenicList'])): $i = 0; $__LIST__ = $auditList['scenicList'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                         <td><?php echo ($vo["supplier_name"]); ?></td>
                         <td>景区</td>
                         <td><?php echo ($vo["supplier_sn"]); ?></td>
                         <td><?php echo ($vo["scenic_name"]); ?>: 儿童价：<?php echo ($vo["child_actual_cost"]); ?>元/人<br>成人价：<?php echo ($vo["adult_actual_cost"]); ?>元/人 </td>
                         <td><?php echo (date("Y-m-d",$vo["start_time"])); ?>至<?php echo (date("Y-m-d",$vo["end_time"])); ?></td>
                         <td><?php echo ($vo["audit_name"]); ?></td>
                         <td>
                             <?php switch($vo["is_review"]): case "1": ?>已审核<?php break;?>
                                 <?php case "-1": ?>未审核<?php break;?>
                                 <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php switch($vo["is_shelves"]): case "1": ?>是<?php break;?>
                                 <?php case "-1": ?>否<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php if(checkAuth('Dealer/Shops/scenicProductById')): ?><a href="<?php echo U('Shops/scenicProductById',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>

                         </td>
                     </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                 <!--保险-->
                 <?php if(is_array($auditList['insureList'])): $i = 0; $__LIST__ = $auditList['insureList'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                         <td><?php echo ($vo["supplier_name"]); ?></td>
                         <td>保险</td>
                         <td><?php echo ($vo["supplier_sn"]); ?></td>
                         <td><?php echo ($vo["insure_name"]); ?>: 儿童价：<?php echo ($vo["child_actual_cost"]); ?>元/人<br>成人价：<?php echo ($vo["adult_actual_cost"]); ?>元/人 </td>
                         <td><?php echo (date("Y-m-d",$vo["start_time"])); ?>至<?php echo (date("Y-m-d",$vo["end_time"])); ?></td>
                         <td><?php echo ($vo["audit_name"]); ?></td>
                         <td>
                             <?php switch($vo["is_review"]): case "1": ?>已审核<?php break;?>
                                 <?php case "-1": ?>未审核<?php break;?>
                                 <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php switch($vo["is_shelves"]): case "1": ?>是<?php break;?>
                                 <?php case "-1": ?>否<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php if(checkAuth('Dealer/Shops/insureProductById')): ?><a href="<?php echo U('Shops/insureProductById',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                         </td>
                     </tr><?php endforeach; endif; else: echo "" ;endif; ?>

                 <!--交通-->
                 <?php if(is_array($auditList['trafficList'])): $i = 0; $__LIST__ = $auditList['trafficList'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                         <td><?php echo ($vo["supplier_name"]); ?></td>
                         <td>交通</td>
                         <td><?php echo ($vo["supplier_sn"]); ?></td>
                         <td><?php echo ($vo["traffic_name"]); ?>: 儿童价：<?php echo ($vo["child_actual_cost"]); ?>元/人<br>成人价：<?php echo ($vo["adult_actual_cost"]); ?>元/人 </td>
                         <td><?php echo (date("Y-m-d",$vo["start_time"])); ?>至<?php echo (date("Y-m-d",$vo["end_time"])); ?></td>
                         <td><?php echo ($vo["audit_name"]); ?></td>
                         <td>
                             <?php switch($vo["is_review"]): case "1": ?>已审核<?php break;?>
                                 <?php case "-1": ?>未审核<?php break;?>
                                 <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php switch($vo["is_shelves"]): case "1": ?>是<?php break;?>
                                 <?php case "-1": ?>否<?php break; endswitch;?>
                         </td>
                         <td>
                             <?php if(checkAuth('Dealer/Shops/trafficProductById')): ?><a href="<?php echo U('Shops/trafficProductById',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                         </td>
                     </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                 </tbody>
             </table>
         </div>
         <!--产品审核结束-->
     </div>

<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Dealer/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>

    <script src="/Public/statics/Dealer/js/highcharts.js"></script>
    <script src="/Public/statics/Dealer/js/highcharts-zh_CN.js"></script>
    <script src="/Public/statics/Dealer/js/exporting.js"></script>

    <script src="/Public/statics/layui/layui.all.js"></script>
    <script src="/Public/statics/layui/layui.js"></script>

    <script>

        layui.use('laydate', function() {
            var laydate = layui.laydate;
            //常规用法
            laydate.render({
                elem: '#toDay'
            });
            laydate.render({
                elem: '#end_time'
            });
        })


        $(function () {
            $('body').on('change','#type',function () {
               var selected=$(this).val();
               if(selected==1){
                   $('#year').show();
                   $('#month').hide();
               }else{
                   $('#year').hide();
                   $('#month').show();
               }
            });


            //饼状图
            $('#pie').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: '<?php echo ($toDay); ?>营业额 <?php echo ($turnover["total"]); ?>'
                },
                tooltip: {
                    headerFormat: '{series.name}<br>',
                    pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '营业额利润比例',
                    data: [
                        ['毛利:<?php echo ((isset($turnover["profit"]) && ($turnover["profit"] !== ""))?($turnover["profit"]):0); ?>元',   <?php echo ((isset($turnover["profitPercent"]) && ($turnover["profitPercent"] !== ""))?($turnover["profitPercent"]):100); ?>],
                        ['成本:<?php echo ((isset($turnover["cost"]) && ($turnover["cost"] !== ""))?($turnover["cost"]):0); ?>元',       <?php echo ((isset($turnover["totalPercent"]) && ($turnover["totalPercent"] !== ""))?($turnover["totalPercent"]):0); ?>],
                    ]
                }]
            });

            //条形图

            /**
             * Highcharts 在 4.2.0 开始已经不依赖 jQuery 了，直接用其构造函数既可创建图表
             **/
            var chart = new Highcharts.Chart('strip', {
                title: {
                    text: '营业收入报表',
                    x: -20
                },
               /* subtitle: {
                    text: '数据来源: WorldClimate.com',
                    x: -20
                },*/
                xAxis: {
                    categories: [<?php if(is_array($report)): $i = 0; $__LIST__ = $report;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>"<?php echo ($vo["name"]); ?>",<?php endforeach; endif; else: echo "" ;endif; ?>]
                },
                yAxis: {
                    title: {
                        text: '金额'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                    //valueSuffix: '元'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: '订单数量',
                    data: [<?php if(is_array($report)): $i = 0; $__LIST__ = $report;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; echo ($vo["orderNum"]); ?>,<?php endforeach; endif; else: echo "" ;endif; ?>]
                }, {
                    name: '毛利',
                    data: [<?php if(is_array($report)): $i = 0; $__LIST__ = $report;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; echo ($vo["profit"]); ?>,<?php endforeach; endif; else: echo "" ;endif; ?>]
                }, {
                    name: '营业额',
                    data: [<?php if(is_array($report)): $i = 0; $__LIST__ = $report;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; echo ($vo["turnover"]); ?>,<?php endforeach; endif; else: echo "" ;endif; ?>]
                }]
            });

        });

    </script>

<script>
    $(function () {
        var bodyH=$(document).height();
        try{
            parent.resetFrameHeight(bodyH);
        } catch(error){

        }
    })
</script>
</body>
</html>