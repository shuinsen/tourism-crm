<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>

    <style>
        #table .btn-primary{ margin-bottom: 5px; }
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button{
            -webkit-appearance: none !important;
            margin: 0;
        }
        .row-2{
            padding: 0px 10px;
        }
        .row-3{
            padding: 2px;
        }

        .form-group{
            padding-right: 50px!important;
            width: 23%!important;
            margin-bottom: 15px!important;
        }
        .create-time-item{
            padding-left: 14px;
        }
        .range-item{
            padding: 7px 10px;
            background-color: #eee;
        }

        .input-left{
            margin-left: -4px;
        }
        .width-600{
            width: 600px!important;
        }
        .margin-left-7{
            margin-left: 7px!important;
        }
        .margin-left-20{
            margin-left: 20px!important;
        }
        .margin-left-28{
            margin-left: 28px!important;
        }
        .margin-left-25{
            margin-left: 25px!important;
        }
        .margin-left-40{
            margin-left: 40px!important;
        }
        .margin-left-47{
            margin-left: 47px!important;
        }
        .margin-left-48{
            margin-left: 48px!important;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
    </style>


    <div class="page-header"><h1>首页 > 财务管理 &gt;销售订单结算</h1></div>
    <ul id="nav" class="nav nav-tabs">
        <li role="presentation"  <?php if($_GET['order_status'] == 1 || !$_GET['order_status']): ?>class="active"<?php endif; ?> ><a href="<?php echo U('settlement_list?order_status=1');?>">待审核</a></li>
        <li role="presentation" <?php if($_GET['order_status'] == 2): ?>class="active"<?php endif; ?>  ><a href="<?php echo U('settlement_list?order_status=2');?>">已审核</a></li>
        <li role="presentation"  <?php if($_GET['order_status'] == -3): ?>class="active"<?php endif; ?> ><a href="<?php echo U('settlement_list?order_status=-3');?>">待退款</a></li>
        <li role="presentation"  <?php if($_GET['order_status'] == -5): ?>class="active"<?php endif; ?> ><a href="<?php echo U('settlement_list?order_status=-5');?>">已退款</a></li>
    </ul>
    <div class="tab-content">
        <div class="row" >
            <form action="" id="settlement-form" class="form-inline"  style="margin-left:10px;" >
                <input type="hidden" name="p" value="1"/>
                <div class="row-2" >
                    <div class="form-group margin-left-28">
                        <label for="order_num">订单号:</label>
                        <input type="text" class="" name="order_num" id="order_num" value="<?php if($_GET['order_num']): echo ($_GET['order_num']); endif; ?>">
                    </div>
                    <div class="form-group margin-left-47">
                        <label for="group_num">团号:</label>
                        <input type="text" class="" name="group_num" id="group_num" value="<?php if($_GET['group_num']): echo ($_GET['group_num']); endif; ?>">
                    </div>
                    <div class="form-group margin-left-7">
                        <label for="line_name">线路名称:</label>
                        <input type="text" class="" name="line_name" id="line_name" value="<?php if($_GET['line_name']): echo ($_GET['line_name']); endif; ?>">
                    </div>
                </div>
                <div class="row-2">
                    <div class="form-group margin-left-28">
                        <label for="order_status">分销商:</label>
                        <select name="reseller_id">
                        <option value="">全部</option>
                        <?php if(is_array($reseller)): foreach($reseller as $key=>$v): ?><option value="<?php echo ($v['reseller_id']); ?>" <?php if($v['reseller_id'] == $_GET['reseller_id']): ?>selected<?php endif; ?>><?php echo ($v['reseller_name']); ?></option><?php endforeach; endif; ?>
                    </select>
                    </div>
                    <div class="form-group margin-left-20">
                        <label for="order_status">直营门店:</label>
                        <select name="shop_id">
                            <option value="">全部</option>
                            <?php if(is_array($shop)): foreach($shop as $key=>$v): ?><option value="<?php echo ($v['reseller_id']); ?>" <?php if($v['reseller_id'] == $_GET['reseller_id']): ?>selected<?php endif; ?>><?php echo ($v['reseller_name']); ?></option><?php endforeach; endif; ?>
                        </select>
                    </div>
                    <div class="form-group margin-left-48">
                        <label for="sales">销售人:</label>
                        <input type="text" class="" name="sales" id="sales" value="<?php if($_GET['sales']): echo ($_GET['sales']); endif; ?>">
                    </div>
                </div>
                <div class="row-2" style="padding: 0 0 20px 15px;">
                    <div class="row">
                        <label for="create_time" class="create-time-item">下单时间:</label>
                        <input type="text" name="start_create_time" id="start" value="<?php if($_GET['start_create_time']): echo ($_GET['start_create_time']); endif; ?>">
                        <span class="range-item input-left">至</span>
                        <input class="input-left" type="text" name="end_create_time" id="end" value="<?php if($_GET['end_create_time']): echo ($_GET['end_create_time']); endif; ?>">
                    </div>
                </div>
                <div class="col-xs-12 " style="padding:0 15px 15px 15px">
                    <a href="<?php echo U('outSettlementListExportExcel?',['order_status'=>$_GET['order_status']]);?>" class="btn btn-primary">导出</a>
                    <button type="submit" class="btn btn-primary submit-btn">查询</button>
                    <div class="dropdown" style="display: inline-block">
                    <button type="button" class="btn btn-primary dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">筛选<span class="caret"></span></button>
                    <ul id="menuList" class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1" style="padding:10px;">
                        <li><input type="checkbox" class="filtrate" checked="" value="0" id="filtrate_0"><label for="filtrate_0">复选框</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="1" id="filtrate_1"><label for="filtrate_1">订单信息</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="2" id="filtrate_2"><label for="filtrate_2">预订人数</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="3" id="filtrate_3"><label for="filtrate_3">门店/分销</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="4" id="filtrate_4"><label for="filtrate_4">销售单价</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="5" id="filtrate_5"><label for="filtrate_5">销售人</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="6" id="filtrate_6"><label for="filtrate_6">结算总金额</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="7" id="filtrate_7"><label for="filtrate_7">币种</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="8" id="filtrate_8"><label for="filtrate_8">汇率</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="9" id="filtrate_9"><label for="filtrate_9">实收人民币</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="10" id="filtrate_10"><label for="filtrate_10">下单时间</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="11" id="filtrate_11"><label for="filtrate_11">操作人</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="12" id="filtrate_12"><label for="filtrate_12">操作</label></li>
                    </ul>
                </div>
                </div>
            </form>
        </div>
        <table id="table" class="table table-striped table-bordered table-hover table-condensed mt-15">
            <thead>
                <tr>
                    <th><input type="checkbox" id="checkAll"/></th>
                    <th>订单信息</th>
                    <th>预订人数</th>
                    <th>门店/分销</th>
                    <!-- <th>订单状态</th> -->
                    <th>销售单价</th>
                    <th>销售人</th>
                    <th>结算总金额</th>
                    <th>币种</th>
                    <th>汇率</th>
                    <th>实收人民币</th>
                    <!-- <th>结算状态</th> -->
                    <th>下单时间</th>
                    <th>操作人</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td><input type="checkbox" id="<?php echo ($v["order_id"]); ?>" name="<?php echo ($v["currency_type"]); ?>" class="checkbox"/></td>
                        <td>
                            订单号：<?php echo ($v["order_num"]); ?><br>
                            团号：<?php echo ($v["group_num"]); ?><br>
                            线路名称：<?php echo ($v["line_name"]); ?>
                        </td>
                        <td>
                            成人：<?php echo ($v["adult_num"]); ?><br>
                            儿童：<?php echo ($v["child_num"]); ?><br>
                            <!--老人：<?php echo ($v["oldMan_num"]); ?><br>-->
                            合计：<?php echo ($v["total_num"]); ?>人
                        </td>
                        <td><?php echo ($v['reseller']); echo ($v['order_type'] == 1 ? '(自营)' : '(分销)'); ?></td>
                        <!-- <td></td> -->
                        <td>
                            成人：<?php echo ($v['adult_price']); ?><br>
                           <!-- 老人：<?php echo ($v['child_price']); ?><br>-->
                            儿童：<?php echo ($v['oldman_price']); ?><br>
                        </td>
                        <td><?php echo ($v['sales']); ?></td>
                        <td><?php echo ($v["total_money"]); ?></td>
                        <td><?php echo ($v["currency_type"]); ?></td>
                        <td><?php echo ($v["settlement_rate"]); ?></td>
                        <td><?php echo ($v['end_need_pay']); ?></td>
                        <td><?php echo ($v['create_time']); ?></td>
                        <td><?php echo ($v['operation']); ?></td>
                        <td>
                            <?php if($v['order_status'] == 1): ?><button onclick="javascript:goSettlement('<?php echo ($v["order_id"]); ?>','<?php echo ($v["currency_type"]); ?>','<?php echo ($v["total_money"]); ?>')" type="button" class="btn btn-primary btn-xs">通过审核</button>

                                <button onclick="javascript:refuseAduit('<?php echo ($v["order_id"]); ?>', 2)" type="button" class="btn btn-primary btn-xs">拒绝审核</button>

                            <?php elseif($v['order_status'] == -3): ?>
                                <button type="submit" class="btn btn-primary btn-xs">确认退款</button><?php endif; ?>
                            <a class="btn btn-primary btn-xs" onclick="openDetail('<?php echo U('Index/orderDetail',array('order_id'=>$v['order_id']));?>')" >详情</a>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                <tr>
                    <td colspan="15">
                        <?php if($_GET['status'] == 0): ?><div class="col-xs-12" >
                                <button type="button" onclick="batchSettlement()" class="btn btn-primary btn-xs">批量确认结算</button>
                            </div><?php endif; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="bjy-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                    <h4 class="modal-title" id="myModalLabel"> 外币结算</h4></div>
                <div class="modal-body">
                    <form id="bjy-form" class="form-inline" action="<?php echo U('Finance/singleForeignSettlement');?>" method="post">
                        <input type="hidden" name="order_id" id="order_id">
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <tr>
                                <th width="17%">结算金额：</th>
                                <td><span class="total_money"></span></td>
                            </tr>
                            <tr>
                                <th>汇率：</th>
                                <td><input class="input-medium" type="text" id="settlement_rate" name="settlement_rate" placeholder="请输入汇率"></td>
                            </tr>
                            <tr>
                                <th>实收金额：</th>
                                <td><input class="input-medium" type="text" id="end_foreign_money" name="end_foreign_money" value="0" placeholder="请输入实际收取的金额"><span class="currency_type"></span></td>
                            </tr>
                            <tr>
                                <th>实收人民币：</th>
                                <td><input class="input-medium" readonly type="text" name="end_need_pay"></td>
                            </tr>
                            <tr>
                                <th></th>
                                <td><input class="btn btn-success" type="submit" value="修改"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
 <ul class="pagination"><?php echo ($show); ?></ul>


<script src="/Tpl/Operator/js/jedate/jedate.js"></script>
    <script>

        /**
         * 订单审核
         * @param  {[int]} oid    [订单id]
         * @param  {[string]} num [订单编号]
         */
        function refuseAduit(oid, type){
            layer.confirm('拒绝该订单通过审核？',{
                btn : [ '拒绝', '返回'],
                title: '订单审核'
            },function(){
                layer.closeAll();
                $.post('<?php echo U('SaleManger/aduit');?>', {order_id : oid, type : type}, function(data){
                    if(data.status ==1 ){
                        layer.msg('操作成功', {icon: 6})
                        setTimeout(function(){
                            location.reload();
                        }, 1000)
                    }else{
                        layer.msg('操作失败', {icon: 5})
                    }
                }, 'json' )
            })
        }

        $('#menuList').on('change', '.filtrate', function(){
            var index = $(this).val();
            if($(this).prop('checked')){
                $('#table tr').each(function(i,n){  $(n).children().eq(index).show(); })
            }else{
                $('#table tr').each(function(i,n){  $(n).children().eq(index).hide();})
            }
        })

        function openDetail(url){
            layer.open({
                type: 2,
                title: '订单详情',
                area: ['100%','100%'],
                content: url
            })
        }
        /**
         * 绑定日期选择器
         * @param  {[obj]}    obj        [元素]
         * @param  {[string]} dateFormat [时间格式]
         */
        function dateFormat(obj, dateFormat){
            jeDate({
                dateCell: '#'+$(obj).attr('id'),
                format: dateFormat,
                isinitVal:false,
                isTime:true, //isClear:false,
                okfun:function(val){
                }
            });
        }
       dateFormat($('#start'), 'YYYY-MM-DD');
       dateFormat($('#end'), 'YYYY-MM-DD');

        // 全选
        $("#checkAll").click(function () {
            if($(this).is(":checked"))
            {
                $(".checkbox").each(function () {
                    $(this).prop('checked',true);
                });
            }else{
                $(".checkbox").each(function () {
                    $(this).prop('checked',false);
                });
            }
        });

        // 汇率绑定值变化的事件
        $("#settlement_rate").bind('input propertychange',function(){
            // console.log($this.val());
            var rate = $('#settlement_rate').val();
            var end_foreign_money=$("#end_foreign_money").val();
            var new_total_money = rate*end_foreign_money;
            // 保留两位小数
            new_total_money = new_total_money.toFixed(2);
            $("input[name='end_need_pay']").val(new_total_money);

        });
        // 外币输入值变化的事件
        $("#end_foreign_money").bind('input propertychange',function(){
            // console.log($this.val());
            var rate = $('#settlement_rate').val();
            var end_foreign_money=$("#end_foreign_money").val();
            var new_total_money = rate*end_foreign_money;
            // 保留两位小数
            new_total_money = new_total_money.toFixed(2);
            $("input[name='end_need_pay']").val(new_total_money);

        });

        // 提交切换订单状态
        function checkStatus(status) {
            $('form')[0].reset();
            $('input[name="status"]').val(status);
            $('#settlement-form').submit();
        }

        // 提交单个结算
        function goSettlement($order_id,$currency_type,$total_money){
            // alert($order_id+$currency_type);
            if ($currency_type == '人民币') {
                            //询问框
                layer.confirm('通过审核?', {
                    btn: ['确定','取消'] //按钮
                }, function(){
                    $.ajax({
                        url:'<?php echo U("Finance/singleSettlementById");?>',
                        dataType:"json",
                        type:'POST',
                        cache:false,
                        data:{order_id:$order_id},
                        success: function(data) {
                            layer.msg('结算成功');
                            if (data.status==1) {
                                setTimeout(function () {
                                    location.reload();
                                },1500);
                            }
                        }
                    });
                });
            }
            // 外币结算窗口
            else{
                $(".total_money").text($total_money);
                $("#order_id").val($order_id);
                $(".currency_type").text($currency_type);
                $('#bjy-edit').modal('show');

            }
        }

        // 批量结算，只能是批量结算人民币的订单 外币则需要汇率
        function batchSettlement(){
            // 批量处理
            var order_ids = '';
            $(".checkbox").each(function () {
                if($(this).is(":checked") && $(this).attr('name')=='人民币')
                {
                    order_ids+=$(this).attr('id')+',';
                }
            });
            // console.log(order_ids);
            if (order_ids == '') {
                layer.alert('请勾选要结算的订单',{icon: 5});
                return;
            }
            layer.confirm('注意：只能批量结算人民币的订单！', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url:'<?php echo U("Finance/batchSettlement");?>',
                    dataType:"json",
                    type:'POST',
                    cache:false,
                    data:{order_ids:order_ids},
                    success: function(data) {
                        layer.msg('结算成功');
                        if (data.status==1) {
                            setTimeout(function () {
                                location.reload();
                            },1500);
                        }
                    }
                });
            });
        }

        // 批量处理
        // $(".chk_0").each(function () {
        //     if($(this).is(":checked"))
        //     {
        //         orderId+=$(this).val()+',';
        //     }
        // });
    </script>