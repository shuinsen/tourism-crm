<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
    <link rel="stylesheet" href="/Public/statics/layui/css/layui.css"  media="all">
    <link rel="stylesheet" type="text/css" href="/Public/plugins/webuploader/webuploader.css" />
    <link rel="stylesheet" type="text/css" href="/Public/plugins/webuploader/style.css" />
    <style>
        .newMess th{text-align:center;}
        th,td{padding: 10px;}
        .row-div {
            height: auto;
            line-height: 30px;
            font-size: 14px;
            margin-bottom: 10px;
            padding: 0px 100px!important;
        }
        .base-info-box{
            margin: 20px 40px;
            height: 40px;
            line-height: 40px;
            background-color: #F6FBFD;
        }
        .header-bg-01{
            width: 2px;
            height: 40px;
            background-color: #5BC0DE;
            display: inline-block;
            float: left;
        }
        .header-title{
            float: left;
            display: inline-block;
            height: 40px;
            padding-left: 20px;
            line-height: 40px;
        }
        .col-md-5 input{
            width: 400px!important;
        }

        .col-md-5 select{
            width: 400px!important;
        }
        .explain{
            font-size: 12px;
            color: #ccc;
            height: 20px;
        }
        .margin-left-20{
            margin-left: 20px!important;
        }
        #wrapper{
            width: 900px!important;
        }
    </style>

</head>
<body>

    <div class="page-header"><h1><i class="fa fa-home"></i> 首页>商家管理>添加注册供应商</h1></div>
     <div class="container-fluid">
         <form action="" method="post">
            <input type="hidden" name="supplier_id" id="supplier_id" value="<?php echo ($supplier_id); ?>">
            <div class="base-info-box">
                    <span class="header-bg-01"></span>
                    <span class="header-title">基本信息</span>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>供应商类型：</p></div>
                <div class="col-md-5">
                    <select id='supplier_type' name="supplier_type">
                        <option value=''>请选择供应商类型</option>
                        <option value="1" <?php if($supplierInfo['supplier_type'] == 1): ?>selected<?php endif; ?>>国内</option>
                        <option value="2" <?php if($supplierInfo['supplier_type'] == 2): ?>selected<?php endif; ?>>国外</option>
                    </select>
                </div>
            </div>
             <div class="row row-div">
                 <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>币种：</p></div>
                 <div class="col-md-5">
                     <select id='currency_id' name="currency_id" required>
                         <option value=''>请选择币种</option>
                         <?php if(is_array($currency)): $i = 0; $__LIST__ = $currency;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["currency_id"]); ?>" <?php if($supplierInfo['currency_id'] == $vo['currency_id']): ?>selected<?php endif; ?>><?php echo ($vo["currency_name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                     </select>
                 </div>
             </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>供应商编号：</p></div>
                <div class="col-md-5">
                    <input class="form-input" <?php if($supplier_id == 0): ?>value="<?php echo ($shopSn); ?>"<?php else: ?>value="<?php echo ($supplierInfo['supplier_sn']); ?>"<?php endif; ?> readonly type="text" name="supplier_sn" id="supplier_sn" placeholder="请输入供应商编号">
                   <!-- <div class="explain">
                        <span>供应商编号用于登录时使用</span>
                    </div>-->
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>供应商账号：</p></div>
                <div class="col-md-5">
                    <input class="form-input" value="<?php echo ($supplierInfo['supplier_account']); ?>" required="required" type="text" name="supplier_account" id="supplier_account" placeholder="请输入供应商账号">
                </div>
            </div>
            <?php if(!$supplierInfo['loginPwd']): ?><div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>登录密码：</p></div>
                    <div class="col-md-5">
                        <input class="form-input" value=""  type="password" name="loginPwd" id="loginPwd " placeholder="请输入登录密码,默认为空则不修改">
                    </div>
                </div><?php endif; ?>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>真实名称：</p></div>
                <div class="col-md-5">
                    <input class="form-input" required  value="<?php echo ($supplierInfo['supplier_name']); ?>" required="required" type="text" name="supplier_name" id="supplier_name" placeholder="请输入供应商真实名称">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>手机号码：</p></div>
                <div class="col-md-5">
                    <input class="form-input"  required value="<?php echo ($supplierInfo['supplier_phone']); ?>" required="required" type="text" name="supplier_phone" id="supplier_phone" maxlength="50">
                    <div class="explain">
                        <span>手机号码用于登录时使用</span>
                    </div>
                </div>

            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>公司座机：</p></div>
                <div class="col-md-5">
                    <input class="form-input"  required value="<?php echo ($supplierInfo['landline_tel']); ?>" required="required" type="text" name="landline_tel" id="landline_tel" placeholder="请输入公司座机">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">邮件地址：</p></div>
                <div class="col-md-5">
                    <input class="form-input"   value="<?php echo ($supplierInfo['qq_email']); ?>" type="text" name="qq_email" id="qq_email" placeholder="请输入邮箱地址">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">性别：</p></div>
                <div class="col-md-6">
                    <input type='radio' width="30" id='supplier_gender1' name='supplier_gender' <?php if($supplierInfo['supplier_gender'] == 1): ?>checked<?php endif; ?> value='1'/><span>男</span>
                    <input style="margin-left: 10px;" type='radio' width="30" id='supplier_gender2' name='supplier_gender' <?php if($supplierInfo['supplier_gender'] == 2): ?>checked<?php endif; ?>  value='2'/><span>女</span>
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">账户状态：</p></div>
                <div class="col-md-6">
                    <input type='radio' width="30" id='supplier_status1' <?php if($supplierInfo['supplier_status'] == -1): ?>checked<?php endif; ?>  name='supplier_status' checked  value='-1'/><span>锁定</span>
                    <input style="margin-left: 10px;" type='radio' width="30" id='supplier_status2' name='supplier_status' <?php if($supplierInfo['supplier_status'] == 1): ?>checked<?php endif; ?> value='1'/><span>正常</span>
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">供应商代码：</p></div>
                <div class="col-md-5">
                    <input class="form-input"   value="<?php echo ($supplierInfo['supplier_code']); ?>" type="text" name="supplier_code" id="supplier_code" placeholder="请输入供应商代码">
                    <div class="explain">
                        <span>供应商代码用于线路团号中识别供应商，如：广州人汇(代码:GZRH)</span>
                    </div>
                </div>
            </div>
            <div class="base-info-box">
                <span class="header-bg-01"></span>
                <span class="header-title">供应商信息</span>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>公司名称：</p></div>
                <div class="col-md-5">
                    <input class="form-input" required  value="<?php echo ($supplierInfo['company_name']); ?>" required="required" type="text" name="company_name" id="company_name" placeholder="请输入公司名称">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">服务等级：</p></div>
                <div class="col-md-5">
                    <select id='service_level' name="service_level"  required >
                        <?php $__FOR_START_1447__=1;$__FOR_END_1447__=6;for($i=$__FOR_START_1447__;$i < $__FOR_END_1447__;$i+=1){ ?><option value="<?php echo ($i); ?>" <?php if($supplierInfo['service_level'] == $i): ?>selected<?php endif; ?>><?php echo ($i); ?>星</option><?php } ?>
                    </select>
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>法人代表：</p></div>
                <div class="col-md-5">
                    <input class="form-input" required  value="<?php echo ($supplierInfo['legal_person']); ?>" required="required" type="text" name="legal_person" id="legal_person" placeholder="请输入公司法人代表">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>联系人：</p></div>
                <div class="col-md-5">
                    <input class="form-input"  required value="<?php echo ($supplierInfo['linkman']); ?>" required="required" type="text" name="linkman" id="linkman" placeholder="请输入联系人">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>传真：</p></div>
                <div class="col-md-5">
                    <input class="form-input" required value="<?php echo ($supplierInfo['fax_number']); ?>" required="required" type="text" name="fax_number" id="fax_number" placeholder="请输入传真">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>所在地：</p></div>
                <div class="col-md-7">
                    国家
                    <select name="country_id" id="ar_country_id"  onchange="getProvince(this,'ar')" required>
                        <option value="">==选择国家==</option>
                        <?php if(is_array($country)): $i = 0; $__LIST__ = $country;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($supplierInfo['country_id'] == $vo['areaId']): ?>selected<?php endif; ?>  value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
                    省
                    <select name="province_id" id="ar_province_id"  onchange="getCity(this,'ar')" required>
                        <option value="">==选择省==</option>
                        <?php if(is_array($supplierInfo["province"])): $i = 0; $__LIST__ = $supplierInfo["province"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option   <?php if($supplierInfo['province_id'] == $vo['areaId']): ?>selected<?php endif; ?>  value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
                    市
                    <select name="city_id" id="ar_city_id"  onchange="getArea(this,'ar')" required>
                        <option value="">==选择市==</option>
                        <?php if(is_array($supplierInfo["city"])): $i = 0; $__LIST__ = $supplierInfo["city"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($supplierInfo['city_id'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
                    区
                    <select name="area_id"   id="ar_area_id"  required>
                        <option value="">==选择区==</option>
                        <?php if(is_array($supplierInfo["area"])): $i = 0; $__LIST__ = $supplierInfo["area"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($supplierInfo['area_id'] == $vo['areaId']): ?>selected<?php endif; ?>  value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>

                    <!--<select id='province_id' onchange='javascript:getAreaList("areaId2",this.value,0)'>
                        <option value=''>请选择</option>
                        <?php if(is_array($areaList)): $i = 0; $__LIST__ = $areaList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value='<?php echo ($vo['areaId']); ?>' <?php if($object['areaId1'] == $vo['areaId'] ): ?>selected<?php endif; ?>><?php echo ($vo['areaName']); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
                    <select id='city_id'  onchange='javascript:getAreaList("areaId3",this.value,1);getCommunitys()'>
                        <option value=''>请选择</option>
                    </select>
                    <select id='area_id' >
                        <option value=''>请选择</option>
                    </select>-->
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">社区/街道：</p></div>
                <div class="col-md-5">
                    <input class="form-input" required value="<?php echo ($supplierInfo['detailed_address']); ?>" type="text" name="detailed_address" id="detailed_address" placeholder="请输入详细地址">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">证件号：</p></div>
                <div class="col-md-5">
                    <input class="form-input" required value="<?php echo ($supplierInfo['id_number']); ?>" type="text" name="id_number" id="id_number" placeholder="请输入证件号码">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">身份证是否认证：</p></div>
                <div class="col-md-6">
                    <input type='radio' width="30"  name='is_certification' checked  value='-1'/><span>未认证</span>
                    <input style="margin-left: 10px;" type='radio' width="30"  name='is_certification' <?php if($supplierInfo['is_certification'] == 1): ?>checked<?php endif; ?> value='1'/><span>已认证</span>
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">排序：</p></div>
                <div class="col-md-5">
                    <input class="form-input" value="<?php echo ($supplierInfo['sort']); ?>" type="text" name="sort" id="sort" placeholder="请输入排序号">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>合作时间：</p></div>
                <div class="col-md-6">
                    <input class="form-input" value="<?php echo ($supplierInfo['start_time']); ?>" style="width: 190px;" required="required" type="text" name="start_time" id="start_time" placeholder="请选择起始时间">
                    至
                    <input class="form-input" value="<?php echo ($supplierInfo['end_time']); ?>" style="width: 190px;" required="required" type="text" name="end_time" id="end_time" placeholder="请选择结束时间">
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right">积分优惠：</p></div>
                <div class="col-md-6">
                    <input type='checkbox' <?php if($supplierInfo['is_integral'] == 1): ?>checked<?php endif; ?> width="30" id='is_integral' name='is_integral' value='1'/><span>享受运营商积分优惠</span>
                </div>
            </div>
            <div class="row row-div">
                <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>公司业务：</p></div>
                <div class="col-md-5">
                    <textarea class="form-input" style="width: 400px;height: 100px;" type="text" name="business_details" id="business_details" placeholder="公司业务详情"><?php echo ($supplierInfo['business_details']); ?></textarea>
                </div>
            </div>

            <div class="base-info-box">
                <span class="header-bg-01"></span>
                <span class="header-title">营业执照</span>
            </div>

             <div class="row row-div">
                 <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>营业执照：</p></div>
                 <div class="col-md-5">
                     <input type="text" name="business_img"  value="<?php echo ($supplierInfo["business_img"]); ?>"  class="pull-left" readonly>
                     <input type="file" class="pull-left" onchange="ajax_upload_file(this.files,this)">
                     <img class="thumbImg pull-right" src="<?php echo ($supplierInfo["business_img"]); ?>" alt="">
                 </div>
             </div>
           <!-- <div id="wrapper">
                <div id="container">
                    &lt;!&ndash;头部，相册选择和格式选择&ndash;&gt;
                    <div id="uploader">
                        <div class="queueList">
                            <div id="dndArea" class="placeholder">
                                <div id="filePicker"></div>
                                <p>或将照片拖到这里，单次最多可选300张</p>
                            </div>
                        </div>
                        <div class="statusBar" style="display:none;">
                            <div class="progress">
                                <span class="text">0%</span>
                                <span class="percentage"></span>
                            </div><div class="info"></div>
                            <div class="btns">
                                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <div style="height: 10px" ></div>
            <div class="col-md-offset-3 col-md-9" style="margin-left: 0px;text-align: center;width: 100%;">
                <input type="hidden" name="is_review" value="<?php echo ((isset($supplierInfo['is_review']) && ($supplierInfo['is_review'] !== ""))?($supplierInfo['is_review']):"-1"); ?>">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>提交
                </button>&nbsp; &nbsp; &nbsp;
                <button class="btn" type="button" onclick="changeReview(-1)">
                    <i class="icon-undo bigger-110"></i>拒绝
                </button>&nbsp; &nbsp; &nbsp;
                <button class="btn btn-info" type="button" onclick="changeReview(1)">
                    <i class="icon-ok bigger-110"></i>通过
                </button>&nbsp; &nbsp; &nbsp;
            </div>
            <div style="height: 200px" ></div>

         </form>
     </div>

<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->


    <script src="/Public/statics/layui/layui.all.js"></script>
    <script src="/Public/statics/layui/layui.js"></script>
    <script type="text/javascript" src="/Public/plugins/webuploader/webuploader.js"></script>
    <script type="text/javascript" src="/Public/plugins/webuploader/upload.js"></script>

    <script>

        function changeReview(status) {
            $('input[name="is_review"]').val(status);
            $('form')[0].submit();
        }
        
        
        layui.use('laydate', function() {
            var laydate = layui.laydate;
            //常规用法
            laydate.render({
                elem: '#start_time'
            });
            laydate.render({
                elem: '#end_time'
            });
        });
    </script>

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>