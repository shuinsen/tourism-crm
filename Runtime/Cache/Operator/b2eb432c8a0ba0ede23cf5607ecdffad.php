<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>

    <style>
        th,td{text-align: center;}
        .row-2{
            padding: 0px 10px;
        }

        .row-input-width{
            width: 347px!important;
        }
        .form-group{
            padding-right: 50px!important;
            width: 42%!important;
            margin-bottom: 15px!important;
        }
        .pull-right{
            margin-right: 14px!important;
        }
        .orderNo-item{
            padding-left: 28px;
        }
        .bill-time-item{
            padding-left: 14px;
        }
        .range-item{
            padding: 7px 10px;
            background-color: #eee;
        }

        .input-left{
            margin-left: -4px;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
    </style>
    <div class="page-header"><h1><i class="fa fa-home"></i> 首页 &gt; 财务管理 &gt; 发票管理</h1></div>
    <div class="col-xs-12">
        <div class="tab-content">
            <div class="row" >
                <form action="<?php echo U('bill_list',['p'=>1]);?>" class="form-inline "  style="margin-left:10px;" >
                    <div class="row-2" >
                        <div class="form-group">
                            <label for="open_time" class="bill-time-item">开票时间:</label>
                            <input type="text" name="start_open_time" id="start" value="<?php if($_GET['start_open_time']): echo ($_GET['start_open_time']); endif; ?>">
                            <span class="range-item input-left">至</span>
                            <input class="input-left" type="text" name="end_open_time" id="end" value="<?php if($_GET['end_open_time']): echo ($_GET['end_open_time']); endif; ?>">
                        </div>
                        <div class="form-group">
                            <label for="open_user" class="bill-time-item">开据人:</label>
                            <input type="text" name="open_user" id="open_user" value="<?php if($_GET['open_user']): echo ($_GET['open_user']); endif; ?>"/>
                        </div>
                     </div>
                    <div class="row-2" >
                        <div class="form-group">
                            <label for="order_num" class="orderNo-item">订单号:</label>
                            <input type="text" class="row-input-width" name="order_num" id="order_num" value="<?php if($_GET['order_num']): echo ($_GET['order_num']); endif; ?>">
                        </div>
                        <div class="form-group">
                            <label for="line_name">线路名称:</label>
                            <input type="text" class="row-input-width" name="line_name" id="line_name" value="<?php if($_GET['line_name']): echo ($_GET['line_name']); endif; ?>">
                        </div>
                    </div>
                    <div class="row-2">
                        <div class="form-group">
                            <label for="tourists_phone">会员手机号:</label>
                            <input type="text" class="row-input-width" name="tourists_phone" id="tourists_phone" value="<?php if($_GET['tourists_phone']): echo ($_GET['tourists_phone']); endif; ?>">
                        </div>
                        <div class="form-group">
                            <label for="receipt_rise">发票抬头:</label>
                            <input type="text" class="row-input-width" name="receipt_rise" id="receipt_rise" value="<?php if($_GET['receipt_rise']): echo ($_GET['receipt_rise']); endif; ?>">
                        </div>

                    </div>
                    <div class="col-xs-12">
                            <a href="<?php echo U('outBillListExportExcel');?>" class="btn btn-primary ">导出</a>
                            <button type="submit" class="btn btn-primary">查询</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="tabbable">
  <!--           <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab">
                <li class="active"><a href="<?php echo U('Rule/admin_user_list');?>">员工列表</a></li>
                <li><a href="<?php echo U('Rule/add_admin');?>">添加员工</a></li>
            </ul> -->
            <div class="tab-content">
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <tr>
                        <th>订单号</th>
                        <th>线路名称</th>
                        <th>销售价</th>
                        <th>发票代码</th>
                        <th>发票号码</th>
                        <th>发票金额</th>
                        <th>发票抬头</th>
                        <th>发票内容</th>
                        <th>开票时间</th>
                        <th>开据人</th>
                        <th>备注</th>
                    </tr>
                    <?php if(is_array($list)): foreach($list as $key=>$v): ?><tr>
                            <td><?php echo ($v['order_num']); ?></td>
                            <td><?php echo ($v['line_name']); ?></td>
                            <td><?php echo ($v['need_pay']); ?></td>
                            <td><?php echo ($v['receipt_code']); ?></td>
                            <td><?php echo ($v['receipt_num']); ?></td>
                            <td><?php echo ($v['receipt_amount']); ?></td>
                            <td><?php echo ($v['receipt_rise']); ?></td>
                            <td><?php echo ($v['receipt_content']); ?></td>
                            <td><?php echo (date('Y-m-d',$v['open_time'])); ?></td>
                            <td><?php echo ($v['open_user']); ?></td>
                            <td><?php echo ($v['remarks']); ?></td>
                        </tr><?php endforeach; endif; ?>
                    <tr>
                        <td colspan="12">
                            <!--分页样式-->
                            <ul class="pagination">
                                <?php echo ($show); ?>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<script src="/Tpl/Operator/js/jedate/jedate.js"></script>
<script>
    dateFormat($('#start'), 'YYYY-MM-DD');
    dateFormat($('#end'), 'YYYY-MM-DD');

    /**
     * 绑定日期选择器
     * @param  {[obj]}    obj        [元素]
     * @param  {[string]} dateFormat [时间格式]
     */
    function dateFormat(obj, dateFormat){
        jeDate({
            dateCell: '#'+$(obj).attr('id'),
            format: dateFormat,
            isinitVal:false,
            isTime:true, //isClear:false,
            okfun:function(val){
            }
        });
    }


    function changeVisitorStatus(uid,status) {
        var operation='开通';
        if(status==0){
            operation='锁定';
        }
        //询问框
        layer.confirm('确定'+operation+'此用户吗?', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url:'<?php echo U("Visitor/changeVisitorStatusById");?>',
                dataType:"json",
                type:'POST',
                cache:false,
                data:{uid:uid,status:status},
                success: function(data) {
                    layer.msg(data.msg);
                    if (data.status==1) {
                        setTimeout(function () {
                            location.reload();
                        },1500);
                    }
                }
            });
        });

    }
</script>